/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

/**
 *
 * @author Chris
 */
public interface GameState {
  public abstract void update();
  public abstract void start();
  public abstract void stop();
}

