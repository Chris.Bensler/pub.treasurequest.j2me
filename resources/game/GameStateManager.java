/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

/**
 *
 * @author Chris
 */
public class GameStateManager {
  public GameState state = null;

  public void update() {
    if (state != null) state.update();
  }

  public void setState(GameState s) {
    if (state != null) state.stop();
    state = s;
    if (state != null) state.start();
  }
}

