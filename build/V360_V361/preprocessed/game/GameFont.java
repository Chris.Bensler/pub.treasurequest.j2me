package game;

/*
 * BitmapFont.java
 *
 * Created on September 25, 2008, 9:17 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.Sprite;

/**
 *
 * @author Chris
 */
public class GameFont {
  private Image font;
  private int fontColor = 0x00FFFFFF;
  private byte[] widthIndex = new byte[256];
  private short[] offsetIndex = new short[256];

  /** Creates a new instance of CustomFont */
  public GameFont(String name, int color) throws Exception {
   int[] rgb;
   int w,h,x,alpha;
    font = Image.createImage(name);
    w = font.getWidth();
    h = font.getHeight();
    rgb = new int[w];
    font.getRGB(rgb,0,w,0,h-1,w,1);
    w = 0;
    x = 0;
    for (int i=32; i < 256; i++) {
      offsetIndex[i] = (short)x;
      w = x;
      alpha = (rgb[x] & 0xFF000000);
      rgb[x] = (rgb[x] & 0x00FFFFFF);
      while ((alpha == 0) || (rgb[x] != 0xFFFFFF)) {
        x++;
        alpha = (rgb[x] & 0xFF000000);
        rgb[x] = (rgb[x] & 0x00FFFFFF);
      }
      x++;
      widthIndex[i] = (byte)(x-w);
    }
    rgb = null;
//    setColor(color);
  }

  public int getColor() {
    return fontColor;
  }

  public void setColor(int color) {
   int[] rgb;
   int w,h,alpha;
    color = (color & 0x00FFFFFF);
    if (fontColor == color) return;
    fontColor = color;
    w = font.getWidth();
    h = font.getHeight();
    rgb = new int[w*h];
    font.getRGB(rgb,0,w,0,0,w,h);
    for (int i=0; i < rgb.length-font.getWidth(); i++) {
      alpha = (rgb[i] & 0xFF000000);
      if ((alpha != 0)) {
        rgb[i] = alpha | color;
      }
    }
    try {
      font = null;
      font = Image.createRGBImage(rgb,w,h,true);
    } catch(Exception e) {
    }
  }

  public void setColor(int r, int g, int b) {
    setColor(r*0x10000 + g*0x100 + b);
  }

  public int textWidth(char c) {
    return widthIndex[c];
  }

  public int textWidth(String txt) {
    int w = 0;
    int i;
    for (i=0; i < txt.length(); i++) {
      w += textWidth(txt.charAt(i));
    }
    return w;
  }
  
  public int textHeight() {
    return font.getHeight()-1;
  }

  public void drawString(Graphics g, String txt, int x, int y, int align) {
    int w,h,i,c;
    w = textWidth(txt);
    h = font.getHeight()-1;
    if ((align & Graphics.LEFT) != 0) {
      // no change
    } else if ((align & Graphics.RIGHT) != 0) {
      x -= w;
    } else if ((align & Graphics.HCENTER) != 0) {
      x -= Math.ceil(w/2);
    // } else { // default to left
      // no change
    }
    if ((align & Graphics.TOP) != 0) {
      // no change
    } else if ((align & Graphics.VCENTER) != 0) {
      y -= Math.ceil(h/2);
    } else if ((align & (Graphics.BOTTOM | Graphics.BASELINE)) != 0) {
      y -= h;
    // } else { // default to top
      // no change
    }
    for (i=0; i < txt.length(); i++) {
      c = txt.charAt(i);
      w = widthIndex[c];
      g.drawRegion(font,offsetIndex[c],0,w,h,Sprite.TRANS_NONE,x,y,0);
      x += w;
    }
  }
}
