package main;

import game.GameFont;
import game.GameClass;
import java.util.Random;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.media.Player;
import game.GameAction;
import game.GameSprite;
import maze.MazeKey;
import maze.MazeMap;
import maze.MazeRoom;
import state.MessageBox;
import state.StateMenu;
import state.StatePlay;

public final class Engine extends GameClass {
 private static TreasureQuest parent = null;
 public static GameFont font1 = null;
// public GameFont font2 = null;
 public static GameFont font3 = null;
 public static Player midi = null;
 public static boolean Sound_Enabled = false;
 public static boolean Music_Enabled = true;

 public static final int FPS = 30;
 public static final int LOGO_TIMEOUT   = 250;
 public static final int SPLASH_TIMEOUT = 5000;

// final LogoStateClass     STATE_LOGO    = new LogoStateClass();
// private SplashStateClass   STATE_SPLASH  = new SplashStateClass();
// final SndInitStateClass  STATE_SNDINIT = new SndInitStateClass();
 public static MessageBox          STATE_INFO;
 public static StateMenu     STATE_MENU;
// final OptionStateClass   STATE_OPTIONS = new OptionStateClass();
// final HelpStateClass     STATE_HELP    = new HelpStateClass();
// final AboutStateClass    STATE_ABOUT   = new AboutStateClass();
 public static StatePlay     STATE_PLAY;

 // common actions
 public static GameAction aUP;
 public static GameAction aDOWN;
 public static GameAction aLEFT;
 public static GameAction aRIGHT;
 public static GameAction aFIRE;
 public static GameAction aMENU;

 public static GameAction[] COMMON_ACTIONS;
 
 private static final int MAX_MAZEX = 14;
 private static final int MAX_MAZEY = 14;
 // RATIOS are int values in the range of 0 to 100 representing a percentage chance of that behaviour
 private static final int DIR_RATIO = 60; // controls the tendency to continue going the same direction
 private static final int VISIT_RATIO = 40; // controls the tendency to visit neighbouring cells, setting this too low will produce small and unusable mazes
 private static final int FILL_RATIO = 60; // controls how much of the specified region it will try to fill in
 private static final int GAP_RATIO = 30; // controls the tendency to create extra gaps (doorways)
 private static final int LOCK_RATIO = 100; // controls the tendency to lock a door (max of 8 locked doors per maze)
 public static final int SEED_VALUE = 2051716863; // any value other than 0 will preset the RNG for the maze
 private static final int MAX_REDO = 10; // number of tries to maximize the fill ratio
 private static final int MIN_AREA = Math.max(MAX_MAZEX, MAX_MAZEY)+1; // minimum number of valid cells

 public static MazeMap maze = null;
 public static MazeRoom room = null;

 private static int viewX=0,viewY=0,viewW,viewH;
 public static Image tsGround,tsFringe,tsItems,tsMAP,tsHUD,tsPC,tsMOB_Skeleton;
 public static Sprite spriteMAP,spriteHUD;
 public static GameSprite pc,mobSkeleton;
 public static Random rand = new Random();

 public static boolean first_pass = true;

 public static int debug_flag = 1;
 public static int map_mode = 0;

  protected Engine(TreasureQuest parent, boolean supress) throws Exception {
    super(supress);
    Engine.parent = parent;
    
    setFPS_MAX(FPS);
    setFPS_MIN(1);
    
    viewW = getWidth();
    viewH = getHeight();
    
    font1 = new GameFont("/font/04.png",0xFFFFFF);
//    font2 = new GameFont("/font/02.png",0xFFFFFF);
    font3 = new GameFont("/font/03.png",0xFFFFFF);
 
    aUP    = new GameAction(getKeyCode(UP));
    aDOWN  = new GameAction(getKeyCode(DOWN));
    aLEFT  = new GameAction(getKeyCode(LEFT));
    aRIGHT = new GameAction(getKeyCode(RIGHT));
    aFIRE  = new GameAction(getKeyCode(FIRE));
    aMENU  = new GameAction(SOFT_LEFT);
    COMMON_ACTIONS = new GameAction[] {aUP,aDOWN,aLEFT,aRIGHT,aFIRE,aMENU};
    SetAction(COMMON_ACTIONS);
    
    STATE_INFO = new MessageBox(this,font3,2,10,width-4,font3.textHeight()*5+8,Graphics.LEFT|Graphics.TOP);
    STATE_MENU = new StateMenu(this);
    STATE_PLAY = new StatePlay(this);
    SetState(STATE_MENU);
  }
  
  protected final void sizeChanged(int w, int h) {
    super.sizeChanged(w, h);
    viewW = w;
    viewH = h;
  }
  
  public final void start() {
    super.start();
  }
  
  public static final void exit() {
    stop();
    tsMAP = null;
    tsHUD = null;
    spriteMAP = null;
    spriteHUD = null;
    parent.notifyDestroyed();
  }

  public static final void clearCanvas(int color) {
   int old_color = gfx.getColor();
    gfx.setColor(color);
    gfx.fillRect(0,0,width,height);
    gfx.setColor(old_color);
  }
  
  public static final void drawSoftButtons(GameFont font, String left, String right) {
   int x,y;
    x = 16;
    y = height-4;
    if (left != null) {
      font.drawString(gfx, left,  x, y, Graphics.HCENTER|Graphics.BOTTOM);
      drawArrow(x,y+1,UP);                   // soft left arrow
    }
    if (right != null) {
      x = width-x;
      font.drawString(gfx, right, x, y, Graphics.HCENTER|Graphics.BOTTOM);
      drawArrow(x,y+1,DOWN);                   // soft right arrow
      
    }
  }

  public static final void newMaze() {
   MazeMap mazeRedo;
   int redo = MAX_REDO;
   int x,y;
    spriteMAP = new Sprite(tsMAP,tsMAP.getWidth()/15,tsMAP.getWidth()/15);
    maze = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,(first_pass && (SEED_VALUE != 0)) ? SEED_VALUE : rand.nextInt());
    while (((redo > 0) || (maze.mazeArea == 1)) && ((MIN_AREA > maze.mazeArea) || (maze.mazeArea < (FILL_RATIO/100 * maze.XMAX * maze.YMAX)))) {
      redo--;
      mazeRedo = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,rand.nextInt());
      if (maze.mazeArea < mazeRedo.mazeArea) {
        maze = null;
        System.gc();
        maze = mazeRedo;
      }
      mazeRedo = null;
      System.gc();
    }
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    // place the pc
    while (true) {
      x = rand.nextInt(room.tlBase.getColumns());
      y = rand.nextInt(room.tlBase.getRows());
      if ((room.tlBase.getCell(x, y) > 1) && (room.tlFringe.getCell(x,y) == 0) && (room.tlItems.getCell(x, y) == 0)) {
        x *= room.tlBase.getCellWidth();
        y *= room.tlBase.getCellHeight();
        pc.setPosition(x,y+room.tlBase.getCellHeight()-pc.getHeight());
        break;
      }
    }
    room.setViewPort(viewX+pc.getX()+pc.getWidth()/2, viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    first_pass = false;
    spriteMAP = null;
    System.gc();
  }

  public static final void drawHUD() {
   int ox,oy,x,y,w,h;
   MazeKey key;
    w = spriteHUD.getWidth();
    h = spriteHUD.getHeight();
    ox = (width-maze.XMAX*w)/2;
    oy = (height-maze.YMAX*h)/2;
    maze.draw(gfx,spriteHUD,ox,oy, debug_flag);

    // mark start room
    gfx.setColor(0xFFFF00);
    gfx.drawRect(ox+maze.cellStart.x*w-1, oy+maze.cellStart.y*h-1, w+1, h+1);

    // mark end room
    gfx.setColor(0x0000FF);
    gfx.drawRect(ox+maze.cellFinish.x*w-1, oy+maze.cellFinish.y*h-1, w+1, h+1);

    // mark current room
    gfx.setColor(0xFFFFFF);
    gfx.drawRect(ox+maze.cellCurrent.x*w-1, oy+maze.cellCurrent.y*h-1, w+1, h+1);

    for (y=0; y < maze.YMAX; y++) {
      for (x=0; x < maze.XMAX; x++) {
        if (maze.Map[y][x].visited) {
          if (debug_flag == 2) {
            gfx.drawString(""+maze.Map[y][x].count, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 3) {
            gfx.drawString(""+maze.Map[y][x].score, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 4) {
            gfx.drawString(""+(maze.Map[y][x].count*maze.Map[y][x].score), ox+x*w-1, oy+y*h-1, 0);
          }
        }
      }
    }

    // mark each door and each key
    for (int i=0; i < maze.keys.size(); i++) {
      gfx.setColor(0xFFFF00);
      key = (MazeKey) maze.keys.elementAt(i);
      if ((debug_flag != 0) || key.drop.visible) {
        x = ox + key.drop.x*w - 1;
        y = oy + key.drop.y*h - 1;
        gfx.drawRect(x+w/2, y+h/2, 1, 1);
      }
      if ((debug_flag != 0) || key.cell1.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell1.x*w - 1;
        y = oy + key.cell1.y*h - 1;
        if ((key.lock1 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock1 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
      if ((debug_flag >= 2) || key.cell2.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell2.x*w - 1;
        y = oy + key.cell2.y*h - 1;
        if ((key.lock2 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock2 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
    }
  }

  private static final class AnimateItem implements Runnable {
   private static int item,tile,last;
   private static MazeKey key;
    public AnimateItem(int item) {
      AnimateItem.item = -(item+1);
      AnimateItem.tile = room.tlItems.getAnimatedTile(AnimateItem.item);
      if (AnimateItem.tile <= 7*3) { // chest
        AnimateItem.last = AnimateItem.tile+2;
        run();
      } else {
        if (AnimateItem.tile >= 25) { // a key
          // add to player inventory
          key = (MazeKey) maze.keys.elementAt(tile-25);
          pc.inventory.addElement(key);
          MessageBox.add(
                    "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it."
                  + "You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it.");
        }
        SetState(STATE_INFO);
        room.tlItems.setAnimatedTile(AnimateItem.item, 0);
      }
    }

    public void run() {
      while (AnimateItem.tile++ != AnimateItem.last) {
        room.tlItems.setAnimatedTile(AnimateItem.item, AnimateItem.tile);
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
      }
    }
  }

  private static final boolean needKey(int dir) {
   MazeKey key = null;
   boolean ret = true;
    if (MazeMap.getLockDir(maze.cellCurrent, dir) == 0) return false; // not locked
    key = (MazeKey) maze.keys.elementAt(MazeMap.getLockKey(maze.cellCurrent, dir));
    // need key
    MessageBox.add(
               "A door blocks your path.\n"
              +"There is a "+key.description+key.name
              +" lock with the number "+(MazeMap.getLockKey(maze.cellCurrent, dir)+1)+" etched into it.");
    if (pc.inventory.indexOf(key) != -1) { // has the key
      pc.inventory.removeElement(key);
      room.doorOpen(dir);
      MessageBox.append("You use the "+key.name+" key to unlock the door.");
      key.cell1.lock -= key.lock1;
      key.cell2.lock -= key.lock2;
      ret = false;
    }
    SetState(STATE_INFO);
    return ret;
  }

  private static final boolean nextRoom() {
   int c = maze.cellCurrent.x;
   int r = maze.cellCurrent.y;
    // check if we need to move to next room
    if (!pc.collidesWith(room.tlDoors, false)) return false;
    if (pc.dir == LEFT) {
      if (needKey(MazeMap.LEFT)) return false;
      c--;
      pc.move(+room.getWidth()-2*room.tlBase.getCellWidth()-pc.getWidth()+4,0);
    } else if (pc.dir == RIGHT) {
      if (needKey(MazeMap.RIGHT)) return false;
      c++;
      pc.move(-room.getWidth()+2*room.tlBase.getCellWidth()+pc.getWidth()-4,0);
    } else if (pc.dir == UP) {
      if (needKey(MazeMap.UP)) return false;
      r--;
      pc.move(0,+room.getHeight()-2*room.tlBase.getCellHeight()-pc.getHeight()+16);
    } else if (pc.dir == DOWN) {
      if (needKey(MazeMap.DOWN)) return false;
      r++;
      pc.move(0,-room.getHeight()+2*room.tlBase.getCellHeight()+pc.getHeight()-16);
    }
    maze.cellCurrent = maze.Map[r][c];
    room = null;
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    return true;
  }

  public static final void movePC(double dx, double dy) {
    if (!pc.idle) {
      pc.move(dx,dy);
      if (!nextRoom() && pc.collidesWith(room.tlFringe, false)) {
        pc.move(-dx,-dy);
      }
      room.setViewPort(viewX+pc.getX()+pc.getWidth()/2,viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    }
  }

  public static final void action_item(int c, int r) {
   // open
   int item = -(room.tlItems.getCell(c, r)+1);
    if (( maze.cellCurrent.items & (1 << item)) == 0) {
      maze.cellCurrent.items |= (1 << item);
      new AnimateItem(item);
    }
  }
  
  /**
   * Copy an int[] by value (deep copy).
   * Avoids use of Object.clone() method (copy by ref / shallow copy)
   * @param src
   * @return 
   */
  public static final int[] copyArray(int[] src) {
   int[] dst = new int[src.length];
    System.arraycopy(src, 0, dst, 0, src.length);
    return dst;
  }

  public static final Object[] copyArray(Object[] src) {
   Object[] dst = new Object[src.length];
    for (int i=0; i < dst.length; i++) {
      if (dst[i] != null) dst[i] = copyArray((int[]) src[i]);
    }
    return dst;
  }
  
  public static final void drawArrow(int x, int y, int dir) {
    if (dir == LEFT) {
      gfx.fillTriangle(x-1, y, x+1, y-2, x+1, y+2);
    } else if (dir == RIGHT) {
      gfx.fillTriangle(x-1, y-2, x+1, y, x-1, y+2);
    } else if (dir == UP) {
      gfx.fillTriangle(x-2, y+1, x, y-1, x+2, y+1);
    } else { // DOWN
      gfx.fillTriangle(x-2, y-1, x+2, y-1, x, y+1);
    }
  }
}
