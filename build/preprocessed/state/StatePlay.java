/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.io.IOException;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import main.Engine;
import game.GameAction;
import game.GameSprite;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class StatePlay extends GameState {

  private final GameAction aMAP = new GameAction(Engine.SOFT_RIGHT);
//#if DefaultConfiguration
  private final GameAction aDEBUG = new GameAction(Engine.KEY_POUND);
//#else
//#    private final GameAction aDEBUG = new GameAction(Engine.KEY_NUM9);
//#endif
  public final GameAction[] ACTION_PLAY = new GameAction[]{aMAP, aDEBUG};
  private Runtime rt = Runtime.getRuntime();
  private long tMem = 0,  fMem = 0,  tick = 0;
  public boolean paused = false;

  public StatePlay(Engine engine) {
    super(engine);
  }

  protected void start() {
    Engine.SetAction(ACTION_PLAY);
    paused = false;
    if (Engine.maze == null) {
      Runtime.getRuntime().gc();
      try {
        Engine.tsMAP = Image.createImage("/img/tset_maze_map.png");
        Engine.tsGround = Image.createImage("/img/ground.png");
        Engine.tsFringe = Image.createImage("/img/fringe.png");
        Engine.tsItems = Image.createImage("/img/items.png");

        Engine.tsPC = Image.createImage("/img/char1.png");
        Engine.pc = new GameSprite(engine, Engine.tsPC, 16, 24);
        Engine.pc.setRefPixelPosition(0, 16);
        Engine.pc.setCollisionRect(2, 16, Engine.pc.getWidth() - 4, 8);
        // walk up,down,left,right
        Engine.pc.MoveAnimate[Engine.UP] = new int[]{0, 1, 2, 1};
        Engine.pc.MoveAnimate[Engine.DOWN] = new int[]{6, 7, 8, 7};
        Engine.pc.MoveAnimate[Engine.LEFT] = new int[]{9, 10, 11, 10};
        Engine.pc.MoveAnimate[Engine.RIGHT] = new int[]{3, 4, 5, 4};
        // stand up,down,left,right
        Engine.pc.IdleAnimate[Engine.UP] = new int[]{1};
        Engine.pc.IdleAnimate[Engine.DOWN] = new int[]{7};
        Engine.pc.IdleAnimate[Engine.LEFT] = new int[]{10};
        Engine.pc.IdleAnimate[Engine.RIGHT] = new int[]{4};
        Engine.pc.setIdle();

        Engine.tsMOB_Skeleton = Image.createImage("/img/skeleton.png");
        Engine.mobSkeleton = new GameSprite(engine, Engine.tsMOB_Skeleton, 20, 24);
        Engine.mobSkeleton.setRefPixelPosition(0, 16);
        Engine.mobSkeleton.setCollisionRect(2, 16, Engine.mobSkeleton.getWidth() - 4, 8);
        Engine.mobSkeleton.MoveAnimate = Engine.pc.MoveAnimate;
        Engine.mobSkeleton.IdleAnimate = Engine.pc.IdleAnimate;
      } catch (Exception e) {
        e.printStackTrace();
        Engine.exit();
      }
      System.gc();
      if (Engine.SEED_VALUE != 0) {
        Engine.rand.setSeed(Engine.SEED_VALUE);
      }
      Engine.pc.start();
      Engine.newMaze();
      System.gc();
    } else if (Engine.STATE_INFO.msgQueue.size() != 0) {
      stateUpdate();
      Engine.SetState(Engine.STATE_INFO);
    }
  }

  protected void stop() {
    Engine.RemoveAction(ACTION_PLAY);
    paused = true;
  }

  protected void stateAction() {
    if (Engine.keyUp(Engine.aMENU)) { // aMENU released
      Engine.STATE_MENU.selected = 0;
      Engine.SetState(Engine.STATE_MENU);
    } else if (Engine.keyUp(aMAP)) { // soft right
      Engine.map_mode++;
      if (Engine.map_mode == 1) {
        try {
          Engine.tsHUD = Image.createImage("/img/tset_maze_hud12.png");
        } catch (IOException ex) {
          ex.printStackTrace();
        }
        Engine.spriteHUD = new Sprite(Engine.tsHUD, Engine.tsHUD.getWidth() / 15, Engine.tsHUD.getWidth() / 15);
      } else {
        Engine.tsHUD = null;
        Engine.spriteHUD = null;
        Engine.map_mode = 0;
      }
    } else if (Engine.keyUp(aDEBUG)) {
      if (++Engine.debug_flag > 4) {
        Engine.debug_flag = 0;
      }
    } else {
      checkKeys();
    }
  }

  private void checkKeys() {
    double dx = 0.0, dy = 0.0;
    if (paused || (Engine.map_mode != 0)) return;
    
    if (Engine.keyDown(Engine.aUP)) {
      Engine.pc.setMove(Engine.UP);
      dy = -GameSprite.SPEED * Engine.fpsScale;
    } else if (Engine.keyDown(Engine.aDOWN)) {
      Engine.pc.setMove(Engine.DOWN);
      dy = GameSprite.SPEED * Engine.fpsScale;
    } else if (Engine.keyDown(Engine.aLEFT)) {
      Engine.pc.setMove(Engine.LEFT);
      dx = -GameSprite.SPEED * Engine.fpsScale;
    } else if (Engine.keyDown(Engine.aRIGHT)) {
      Engine.pc.setMove(Engine.RIGHT);
      dx = GameSprite.SPEED * Engine.fpsScale;
    } else {
      Engine.pc.setIdle();
      if (Engine.keyUp(Engine.aFIRE)) {
        int w, h, c, r;
        int[] collide = Engine.pc.getCollisionRect();
        int[] rect = new int[4];
        w = Engine.room.tlBase.getCellWidth();
        h = Engine.room.tlBase.getCellHeight();
        rect[0] = Engine.pc.getX() - Engine.room.getX() + collide[0] + (collide[2] / 2) - 1; // sprite axis
        rect[1] = Engine.pc.getY() - Engine.room.getY() + collide[1] + (collide[3] / 2) - 1;  // sprite axis
        rect[0] /= w;
        rect[1] /= h;
        for (r = rect[1] - 1; r <= rect[1] + 1; r++) {
          for (c = rect[0] - 1; c <= rect[0] + 1; c++) {
            if ((((Engine.pc.dir == Engine.LEFT) && (c == rect[0] - 1)) || ((Engine.pc.dir == Engine.RIGHT) && (c == rect[0] + 1)) || ((Engine.pc.dir == Engine.UP) && (r == rect[1] - 1)) || ((Engine.pc.dir == Engine.DOWN) && (r == rect[1] + 1)) || ((c == rect[0]) && (r == rect[1])) // tile the pc is standing on
                    ) && (Engine.room.tlItems.getCell(c, r) < 0)) { // found something
              Engine.action_item(c, r);
              return;
            }
          }
        }
      }
    }
    Engine.movePC(dx, dy);
  }

  protected void stateUpdate() {
    if (Engine.first_pass) {
      return;
    }
    Engine.clearCanvas(0x000000);
    if (Engine.map_mode != 0) {
      Engine.drawHUD();
    } else {
      Engine.room.paint(Engine.gfx, 0, 0);
    }
    Engine.gfx.setColor(0xFFFFFF);
    Engine.font1.drawString(Engine.gfx, Long.toString((long) Engine.fps), 1, 2, Graphics.LEFT | Graphics.TOP);
    int i = 1;
    if (Engine.debug_flag == 1) {
      Engine.font1.drawString(Engine.gfx, (tMem - fMem) + "/" + tMem + " (" + fMem + " free)", 1, Engine.height - 10 * i++, Graphics.LEFT | Graphics.BOTTOM);
      Engine.font1.drawString(Engine.gfx, "seed = " + Engine.maze.SEED_VALUE, 1, Engine.height - 10 * i++, Graphics.LEFT | Graphics.BOTTOM);
      Engine.font1.drawString(Engine.gfx, "Map[" + Engine.maze.cellCurrent.y + "][" + Engine.maze.cellCurrent.x + "]", 1, Engine.height - 10 * i++, Graphics.LEFT | Graphics.BOTTOM);
    }
    Engine.drawSoftButtons(Engine.font1, "MENU", "MAP");
    engine.flushGraphics();
    tick++;
    if (tick > 100) {
      tick = 0;
      rt.gc();
      tMem = rt.totalMemory();
      fMem = rt.freeMemory();
    }
  }
}
