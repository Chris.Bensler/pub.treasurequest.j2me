/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package state;

import game.GameState;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import main.Engine;

/**
 *
 * @author Chris
 */
public final class StateLogo extends GameState {
 private final Timer timer = new Timer();
 private Image logo = null;
 private final int MAX_FRAME = (int) (90.0*(Engine.FPS/30.0));
 private int frame = 0;
 private int state = 0;
 
  public StateLogo(Engine engine) {
    super(engine);
  }

  protected void start() {
    try {
      logo = Image.createImage("/img/retro-action games.png");
    } catch (IOException e) {
      System.out.println("Failed to load logo");
    }
//      font3.setColor(0xEE0000);
  }

  protected final void stop() {
//      font3.setColor(0xFFFFFF);
    System.gc();
  }

  private final class LogoTimer extends TimerTask {
    public final void run() {
      engine.SetState(new StateSplash(engine));
    }
  }

  protected final void stateAction() {}
  protected final void stateUpdate() {
    engine.clearCanvas(0x000000);
    if (state == 0) {
      if (frame < MAX_FRAME) {
        frame++;
        if (frame == MAX_FRAME) state++;
      }
    } else {
      if (frame > 0) {
        frame--;
        if (frame == 0) timer.schedule(new LogoTimer(), Engine.LOGO_TIMEOUT);
      }
      engine.font3.drawString(engine.gfx, "PRESENTS", engine.getWidth()/2, engine.getHeight()/2, Graphics.HCENTER|Graphics.VCENTER);
    }
    for (int i=0; i < frame/(MAX_FRAME/30); i++) {
      try {
        engine.gfx.drawImage(logo, engine.getWidth() / 2, engine.getHeight() / 2, Graphics.HCENTER | Graphics.VCENTER);
      } catch (NullPointerException e) { // if logo is null
      }
    }
    engine.flushGraphics();
  }
}
