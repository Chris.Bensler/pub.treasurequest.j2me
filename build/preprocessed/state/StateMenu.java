/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package state;

import javax.microedition.lcdui.Graphics;
import main.Engine;
import game.GameFont;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class StateMenu extends GameState {
 private final Object mlNEW     = "NEW GAME";
 private final Object mlRESUME   = "RESUME";
 private final Object mlOPTIONS  = "OPTIONS";
 private final Object mlHELP     = "HELP";
 private final Object mlABOUT    = "ABOUT";
 private final Object mlEXIT     = "EXIT";
 private Object[] MenuList;

 public int selected = 0;

  public StateMenu(Engine engine) { super(engine); }

  protected void start() {
    if (engine.maze == null) {
      MenuList = new Object[] {mlNEW,mlOPTIONS,mlHELP,mlEXIT,mlABOUT};
    } else {
      MenuList = new Object[] {mlRESUME,mlNEW,mlOPTIONS,mlHELP,mlEXIT,mlABOUT};
    }
  }

  protected void stateAction() {
    if (Engine.keyUp(engine.aUP)) {
      selected--;
      if (selected < 0) selected += MenuList.length;
    } else if (Engine.keyUp(engine.aDOWN)) {
      selected++;
      if (selected >= MenuList.length) selected -= MenuList.length;
    } else if (Engine.keyUp(engine.aFIRE)) {
      if (MenuList[selected] == mlRESUME) {
        engine.SetState(engine.STATE_PLAY);
      } else if (MenuList[selected] == mlNEW) {
        if (engine.maze != null) { // confirm terminate current game
          // save option?
          engine.newMaze();
        }
        engine.SetState(engine.STATE_PLAY);
      } else if (MenuList[selected] == mlOPTIONS) {
        engine.SetState(new StateOptions(engine));
      } else if (MenuList[selected] == mlHELP) {
        engine.SetState(new StateHelp(engine));
      } else if (MenuList[selected] == mlABOUT) {
        engine.SetState(new StateAbout(engine));
      } else if (MenuList[selected] == mlEXIT) {
        engine.exit();
      }
    }
  }

  protected void stateUpdate() {
   GameFont font = null;
   String[] ml = new String[5];
   int y,ms;
    engine.clearCanvas(0x000000);
    y = (engine.getHeight()/2)-(engine.font1.textHeight()*(MenuList.length-1))/2;
    for (int i=0; i < ml.length; i++) {
      ms = selected-2+i;
      if (ms < 0) ms += MenuList.length;
      if (ms >= MenuList.length) ms -= MenuList.length;
      ml[i] = (String) MenuList[ms];
    }
    for (int i=0; i < ml.length; i++) {
      font = (i != 2) ? engine.font1 : engine.font3;
      font.drawString(engine.gfx,(String) ml[i],engine.getWidth()/2,y,Graphics.HCENTER|Graphics.TOP);
      y += font.textHeight();
    }
    engine.flushGraphics();
  }
}
