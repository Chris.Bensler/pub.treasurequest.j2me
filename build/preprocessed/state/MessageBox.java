/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package state;

import main.*;
import game.GameFont;
import game.GameState;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Chris
 */
public final class MessageBox extends GameState {
 private static final int BG_COLOR = 0x444444;
 private static final int FG_COLOR = 0xDDDDDD;
 public static Vector msgQueue = new Vector();
 private static GameFont font;
 private static int scroll = 0;
 private static int maxy = 0;
 private static int x,y,w,h,align;

  public MessageBox(Engine engine, GameFont font, int x, int y, int w, int h, int align) {
    super(engine);
    this.font = font;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  public void start() {
    Engine.STATE_PLAY.paused = true;
  }
  
  public static void add(String txt) {
    msgQueue.addElement(txt);
  }
  
  public static void append(String txt) {
    if (msgQueue.size() != 0) {
      txt = (String) msgQueue.lastElement() + "\n" + txt;
      msgQueue.removeElementAt(msgQueue.size()-1);
    }
    add(txt);
  }

  public final void stateAction() {
    if (msgQueue.size() == 0) return;
    if (Engine.keyUp(Engine.aMENU)) {
      Engine.SetState(Engine.STATE_MENU);
    } else if (Engine.keyDown(Engine.aUP)) { // scroll up
      if (scroll != 0) scroll--;
    } else if (Engine.keyDown(Engine.aDOWN)) { // scroll down
      if (scroll != maxy) scroll++;
    } else if (Engine.keyUp(Engine.aFIRE)) { // close window
      msgQueue.removeElementAt(0);
      scroll = 0;
      maxy = 0;
      if (msgQueue.size() == 0) {
        Engine.STATE_PLAY.paused = false;
        Engine.SetState(Engine.STATE_PLAY);
      }
    }
  }

  public final void stateUpdate() {
   Vector msg;
   int x,y,w,h;
    if (msgQueue.size() == 0) return;

    align = (align & (Graphics.LEFT|Graphics.RIGHT)) | Graphics.TOP; // force top alignment, retain specified horizontal alignment

    x = this.x;
    y = this.y;
    w = this.w;
    h = this.h;

    Engine.gfx.setColor(BG_COLOR);
    Engine.gfx.fillRect(x, y, w, h);

    Engine.gfx.setColor(FG_COLOR);
    Engine.gfx.drawRect(x, y, w, h);

    x += 2; y += 2; w -= 4; h -= 4;
    Engine.gfx.drawRect(x, y, w, h);

    x += 2; y += 2; w -= 4; h -= 4;
    msg = wrapText((String) msgQueue.elementAt(0),w);
    maxy = msg.size()*font.textHeight() - h;
    if (maxy > 0) {
      Engine.gfx.setColor(BG_COLOR);
      if (scroll > 0) Engine.gfx.fillRect(x+w/2-7, y+h+2, 9, 3);
      if (scroll < maxy) Engine.gfx.fillRect(x+w/2-1, y+h+2, 9, 3);
      Engine.gfx.setColor(FG_COLOR);
      if (scroll > 0) Engine.drawArrow(x+w/2-3, y+h+3, Engine.UP); // scroll up arrow
      if (scroll < maxy) Engine.drawArrow(x+w/2+3, y+h+3, Engine.DOWN); // scroll down arrow
      y -= scroll;
    } else {
      maxy = 0;
    }

    Engine.gfx.setClip(x, y+scroll, w, h+1);

    for (int i=0; i < msg.size(); i++) {
      font.drawString(Engine.gfx, (String) msg.elementAt(i), x, y+(i*font.textHeight()), align);
    }
    Engine.gfx.setClip(0, 0, Engine.width, Engine.height);
    engine.flushGraphics(this.x,this.y,this.w+1,this.h+1);
  }
  
  private static final Vector wrapText(String txt, int w) {
   Vector msg = new Vector();
   int start = 0, stop = 0, lw = 0;
   char c;
    for (int i=0; i < txt.length(); i++) {
      c = txt.charAt(i);
      lw += font.textWidth(c);
      if (c <= ' ') stop = i;
      if ((lw >= w) || (c == '\n')) {
        if (stop == start) stop = i;
        msg.addElement(txt.substring(start, stop++));
//        if ((c == '\n') || (stop != i)) stop++;
        start = stop;
        lw = 0;
        if (start <= i) {
          lw = font.textWidth(txt.substring(start, i));
        }
      }
    }
    if (start != txt.length()) msg.addElement(txt.substring(start, txt.length()));
    return msg;
  }
}
