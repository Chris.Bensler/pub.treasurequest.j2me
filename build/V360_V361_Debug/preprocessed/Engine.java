import game.GameFont;
import game.GameClass;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import maze.MazeKey;
import maze.MazeMap;
import maze.MazeRoom;

public class Engine extends GameClass {
 private TreasureQuest parent = null;
 private GameFont font1 = null;
// public GameFont font2 = null;
 private GameFont font3 = null;
 private Player midi = null;
 private boolean Sound_Enabled = false;
 private boolean Music_Enabled = true;

 private static final int FPS = 30;
 private static final int LOGO_TIMEOUT   = 250;
 private static final int SPLASH_TIMEOUT = 5000;

// final LogoStateClass     STATE_LOGO    = new LogoStateClass();
// final SplashStateClass   STATE_SPLASH  = new SplashStateClass();
// final SndInitStateClass  STATE_SNDINIT = new SndInitStateClass();
 private MenuStateClass     STATE_MENU    = new MenuStateClass();
// final OptionStateClass   STATE_OPTIONS = new OptionStateClass();
// final HelpStateClass     STATE_HELP    = new HelpStateClass();
// final AboutStateClass    STATE_ABOUT   = new AboutStateClass();
 protected RunStateClass    STATE_PLAY    = new RunStateClass();

 // common actions
 private final GameAction aUP    = new GameAction(getKeyCode(UP));
 private final GameAction aDOWN  = new GameAction(getKeyCode(DOWN));
 private final GameAction aLEFT  = new GameAction(getKeyCode(LEFT));
 private final GameAction aRIGHT = new GameAction(getKeyCode(RIGHT));
 private final GameAction aFIRE  = new GameAction(getKeyCode(FIRE));
 private final GameAction aMENU  = new GameAction(SOFT_LEFT);

 private final GameAction[] COMMON_ACTIONS = new GameAction[] {aUP,aDOWN,aLEFT,aRIGHT,aFIRE,aMENU};
 
 private static final int MAX_MAZEX = 14;
 private static final int MAX_MAZEY = 14;
 // RATIOS are int values in the range of 0 to 100 representing a percentage chance of that behaviour
 private static final int DIR_RATIO = 60; // controls the tendency to continue going the same direction
 private static final int VISIT_RATIO = 40; // controls the tendency to visit neighbouring cells, setting this too low will produce small and unusable mazes
 private static final int FILL_RATIO = 60; // controls how much of the specified region it will try to fill in
 private static final int GAP_RATIO = 30; // controls the tendency to create extra gaps (doorways)
 private static final int LOCK_RATIO = 100; // controls the tendency to lock a door (max of 8 locked doors per maze)
 private static final int SEED_VALUE = 2051716863; // any value other than 0 will preset the RNG for the maze
 private static final int MAX_REDO = 10; // number of tries to maximize the fill ratio
 private static final int MIN_AREA = Math.max(MAX_MAZEX, MAX_MAZEY)+1; // minimum number of valid cells

 private MazeMap maze = null;
 private MazeRoom room = null;

 private int viewX=0,viewY=0,viewW,viewH;
 private Image tsGround,tsFringe,tsItems,tsMAP,tsHUD,tsPC,tsMOB_Skeleton;
 private Sprite spriteMAP,spriteHUD;
 private GameSprite pc,mobSkeleton;
 private Random rand = new Random();
 private Timer timer = null;

 private boolean first_pass = true;

 private Vector txtGameInfo = new Vector();
 private int debug_flag = 1;
 private int map_mode = 0;

  protected Engine(TreasureQuest parent, boolean supress) throws Exception {
    super(supress);
    System.err.println(System.getProperty("UID"));
    System.err.println(System.getProperty("SOFT1"));
    this.parent = parent;
    setFPS_MAX(FPS);
    setFPS_MIN(1);
    font1 = new GameFont("/font/04.png",0xFFFFFF);
//    font2 = new GameFont("/font/02.png",0xFFFFFF);
    font3 = new GameFont("/font/03.png",0xFFFFFF);
    viewW = getWidth();
    viewH = getHeight();
    SetAction(COMMON_ACTIONS);
    SetState(STATE_MENU);//new LogoStateClass());
  }
  
  protected void sizeChanged(int w, int h) {
    super.sizeChanged(w, h);
    viewW = w;
    viewH = h;
  }
  
  public void start() {
    super.start();
    timer = new Timer();
  }
  
  private void exit() {
    stop();
    tsMAP = null;
    tsHUD = null;
    spriteMAP = null;
    spriteHUD = null;
    try { parent.destroyApp(false); } catch(Exception e) { }
    parent = null;
  }

  private void clearCanvas(int color) {
   int old_color = gfx.getColor();
    gfx.setColor(color);
    gfx.fillRect(0,0,getWidth(),getHeight());
    gfx.setColor(old_color);
  }
  
  private void drawSoftButtons(GameFont font, String left, String right) {
   int x,y;
    x = 16;
    y = getHeight()-4;
    if (left != null) {
      font.drawString(gfx, left,  x, y, Graphics.HCENTER|Graphics.BOTTOM);
      gfx.fillTriangle(x-2,y,x+2,y,x,y+2);                   // soft left arrow
    }
    if (right != null) {
      x = getWidth()-x;
      font.drawString(gfx, right, x, y, Graphics.HCENTER|Graphics.BOTTOM);
      gfx.fillTriangle(x-2,y,x+2,y,x,y+2);                   // soft right arrow
    }
  }

  private static int drawWrapped(GameFont font, Graphics gfx, String txt, int x, int y, int w, int align) {
   int start = 0, stop = 0, iy = y, ww = 0;
   char c;
    align = (align & (Graphics.LEFT|Graphics.RIGHT)) | Graphics.TOP; // force top alignment, retain specified horizontal alignment
    for (int i=0; i < txt.length(); i++) {
      c = txt.charAt(i);
      ww += font.textWidth(c);
      if (c <= ' ') stop = i;
      if ((ww >= w) || (c == '\n')) {
        if (stop == start) stop = i;
        font.drawString(gfx,txt.substring(start, stop), x, (iy += font.textHeight()), align);
        if ((c == '\n') || (stop != i)) stop++;
        start = stop;
        ww = 0;
        if (start <= i) {
          ww = font.textWidth(txt.substring(start, i));
        }
      }
    }
    if (start != txt.length()) font.drawString(gfx,txt.substring(start, txt.length()), x, (iy += font.textHeight()), align);
    return iy;
  }

  private void newMaze() {
   MazeMap mazeRedo;
   int redo = MAX_REDO;
   int x,y;
    spriteMAP = new Sprite(tsMAP,tsMAP.getWidth()/15,tsMAP.getWidth()/15);
    maze = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,(first_pass && (SEED_VALUE != 0)) ? SEED_VALUE : rand.nextInt());
    while (((redo > 0) || (maze.mazeArea == 1)) && ((MIN_AREA > maze.mazeArea) || (maze.mazeArea < (FILL_RATIO/100 * maze.XMAX * maze.YMAX)))) {
      redo--;
      mazeRedo = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,rand.nextInt());
      if (maze.mazeArea < mazeRedo.mazeArea) {
        maze = null;
        System.gc();
        maze = mazeRedo;
      }
      mazeRedo = null;
      System.gc();
    }
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    // place the pc
    while (true) {
      x = rand.nextInt(room.tlBase.getColumns());
      y = rand.nextInt(room.tlBase.getRows());
      if ((room.tlBase.getCell(x, y) > 1) && (room.tlFringe.getCell(x,y) == 0) && (room.tlItems.getCell(x, y) == 0)) {
        x *= room.tlBase.getCellWidth();
        y *= room.tlBase.getCellHeight();
        pc.setPosition(x,y+room.tlBase.getCellHeight()-pc.getHeight());
        break;
      }
    }
    room.setViewPort(viewX+pc.getX()+pc.getWidth()/2, viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    first_pass = false;
    spriteMAP = null;
    System.gc();
  }

  private void drawHUD() {
   int ox,oy,x,y,w,h;
   MazeKey key;
    w = spriteHUD.getWidth();
    h = spriteHUD.getHeight();
    ox = (getWidth()-maze.XMAX*w)/2;
    oy = (getHeight()-maze.YMAX*h)/2;
    maze.draw(gfx,spriteHUD,ox,oy, debug_flag);

    // mark start room
    gfx.setColor(0xFFFF00);
    gfx.drawRect(ox+maze.cellStart.x*w-1, oy+maze.cellStart.y*h-1, w+1, h+1);

    // mark end room
    gfx.setColor(0x0000FF);
    gfx.drawRect(ox+maze.cellFinish.x*w-1, oy+maze.cellFinish.y*h-1, w+1, h+1);

    // mark current room
    gfx.setColor(0xFFFFFF);
    gfx.drawRect(ox+maze.cellCurrent.x*w-1, oy+maze.cellCurrent.y*h-1, w+1, h+1);

    for (y=0; y < maze.YMAX; y++) {
      for (x=0; x < maze.XMAX; x++) {
        if (maze.Map[y][x].visited) {
          if (debug_flag == 2) {
            gfx.drawString(""+maze.Map[y][x].count, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 3) {
            gfx.drawString(""+maze.Map[y][x].score, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 4) {
            gfx.drawString(""+(maze.Map[y][x].count*maze.Map[y][x].score), ox+x*w-1, oy+y*h-1, 0);
          }
        }
      }
    }

    // mark each door and each key
    for (int i=0; i < maze.keys.size(); i++) {
      gfx.setColor(0xFFFF00);
      key = (MazeKey) maze.keys.elementAt(i);
      if ((debug_flag != 0) || key.drop.visible) {
        x = ox + key.drop.x*w - 1;
        y = oy + key.drop.y*h - 1;
        gfx.drawRect(x+w/2, y+h/2, 1, 1);
      }
      if ((debug_flag != 0) || key.cell1.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell1.x*w - 1;
        y = oy + key.cell1.y*h - 1;
        if ((key.lock1 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock1 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
      if ((debug_flag >= 2) || key.cell2.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell2.x*w - 1;
        y = oy + key.cell2.y*h - 1;
        if ((key.lock2 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock2 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
    }
  }

  private class GameInfoTimeout extends TimerTask {
    public void run() {
      txtGameInfo.removeAllElements();
    }
  }

  private class DoorLockedTimeout extends GameInfoTimeout {
    public void run() {
      super.run();
      door_locked_wait = false;
    }
  }

  private class AnimateItem implements Runnable {
   private int item,tile,last;
   private MazeKey key;
    public AnimateItem(int item) {
      this.item = -(item+1);
      this.tile = room.tlItems.getAnimatedTile(this.item);
      if (this.tile <= 7*3) { // chest
        this.last = this.tile+2;
        run();
      } else {
        if (this.tile >= 25) { // a key
          // add to player inventory
          key = (MazeKey) maze.keys.elementAt(tile-25);
          pc.inventory.addElement(key);
          txtGameInfo.addElement("You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it.");
          timer.schedule(new GameInfoTimeout(), 3000);
        }
        room.tlItems.setAnimatedTile(this.item, 0);
      }
    }

    public void run() {
      while (this.tile++ != this.last) {
        room.tlItems.setAnimatedTile(this.item, this.tile);
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
      }
    }
  }

  private boolean door_locked_wait = false;
  private boolean needKey(int dir) {
   MazeKey key = null;
    if (MazeMap.getLockDir(maze.cellCurrent, dir) == 0) return false; // not locked
    key = (MazeKey) maze.keys.elementAt(MazeMap.getLockKey(maze.cellCurrent, dir));
    // need key
    if (!door_locked_wait) {
      txtGameInfo.addElement("A door blocks your path.\n"
              +"There is a "+key.description+key.name
              +" lock with the number "+(MazeMap.getLockKey(maze.cellCurrent, dir)+1)+" etched into it.");
      door_locked_wait = true;
      timer.schedule(new DoorLockedTimeout(), 3000);
    }
    if (pc.inventory.indexOf(key) != -1) { // has the key
      pc.inventory.removeElement(key);
      room.doorOpen(dir);
      txtGameInfo.addElement("You use the "+key.name+" key to unlock the door.");
      timer.schedule(new DoorLockedTimeout(), 3000);
      key.cell1.lock -= key.lock1;
      key.cell2.lock -= key.lock2;
      return false;
    }
    return true;
  }

  private boolean nextRoom() {
   int c = maze.cellCurrent.x;
   int r = maze.cellCurrent.y;
    // check if we need to move to next room
    if (!pc.collidesWith(room.tlDoors, false)) return false;
    if (pc.dir == LEFT) {
      if (needKey(MazeMap.LEFT)) return false;
      c--;
      pc.move(+room.getWidth()-2*room.tlBase.getCellWidth()-pc.getWidth()+4,0);
    } else if (pc.dir == RIGHT) {
      if (needKey(MazeMap.RIGHT)) return false;
      c++;
      pc.move(-room.getWidth()+2*room.tlBase.getCellWidth()+pc.getWidth()-4,0);
    } else if (pc.dir == UP) {
      if (needKey(MazeMap.UP)) return false;
      r--;
      pc.move(0,+room.getHeight()-2*room.tlBase.getCellHeight()-pc.getHeight()+16);
    } else if (pc.dir == DOWN) {
      if (needKey(MazeMap.DOWN)) return false;
      r++;
      pc.move(0,-room.getHeight()+2*room.tlBase.getCellHeight()+pc.getHeight()-16);
    }
    maze.cellCurrent = maze.Map[r][c];
    room = null;
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    return true;
  }

  private void movePC(double dx, double dy) {
    if (!pc.idle) {
      pc.move(dx,dy);
      if (!nextRoom() && pc.collidesWith(room.tlFringe, false)) {
        pc.move(-dx,-dy);
      }
      room.setViewPort(viewX+pc.getX()+pc.getWidth()/2,viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    }
  }

  private void action_item(int c, int r) {
   // open
   int item = -(room.tlItems.getCell(c, r)+1);
    if (( maze.cellCurrent.items & (1 << item)) == 0) {
      maze.cellCurrent.items |= (1 << item);
      new AnimateItem(item);
    }
  }

  //-----------------------------------------------------------------------//
  // Logo State
  private class LogoStateClass extends GameState {
   private Timer timer = new Timer();
   private Image logo;
   private final int MAX_FRAME = 90*(FPS/30);
   private int frame = 0;
   private int state = 0;
    protected void start() {
      font3.setColor(0xEE0000);
      try {
        logo = Image.createImage("/img/retro-action games.png");
      } catch (IOException e) {
        System.out.println("Failed to load logo");
      }
    }
    
    protected void stop() {
      timer = null;
      logo = null;
      font3.setColor(0xFFFFFF);
      System.gc();
    }

    private class LogoTimer extends TimerTask {
      public void run() {
        SetState(new SplashStateClass());
      }
    }

    protected void stateAction() {}
    protected void stateUpdate() {
      clearCanvas(0x000000);
      if (state == 0) {
        if (frame < MAX_FRAME) {
          frame++;
          if (frame == MAX_FRAME) state++;
        }
      } else {
        if (frame > 0) {
          frame--;
          if (frame == 0) timer.schedule(new LogoTimer(), LOGO_TIMEOUT);
        }
        font3.drawString(gfx, "PRESENTS", getWidth()/2, getHeight()/2, Graphics.HCENTER|Graphics.VCENTER);
      }
      for (int i=0; i < frame/(MAX_FRAME/30); i++) gfx.drawImage(logo, getWidth()/2, getHeight()/2, Graphics.HCENTER|Graphics.VCENTER);
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Splash State
  private class SplashStateClass extends GameState {
   private Timer timer = new Timer();
   private Image splash;
    protected void start() {
      try {
        splash = Image.createImage("/img/treasurequest.png");
      } catch (IOException e) {
        System.out.println("Failed to load splash");
      }
      timer.schedule(new SplashTimer(), SPLASH_TIMEOUT);
    }
    
    protected void stop() {
      splash = null;
      timer = null;
      Runtime.getRuntime().gc();
    }

    private class SplashTimer extends TimerTask {
      public void run() {
        SetState(new SndInitStateClass());
      }
    }

    protected void stateAction() {}
    protected void stateUpdate() {
      clearCanvas(0x000000);
      gfx.drawImage(splash, getWidth()/2, getHeight()/2, Graphics.HCENTER|Graphics.VCENTER);
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Sound Option State
  private class SndInitStateClass extends GameState {
   private final GameAction aNO = new GameAction(SOFT_LEFT);
   private final GameAction aYES = new GameAction(SOFT_RIGHT);
   private final GameAction[] ACTION_CONFIRM = new GameAction[] {aNO,aYES};

    protected void start() {
      try {
        InputStream is = getClass().getResourceAsStream("/snd/01.mid");
        midi = Manager.createPlayer(is, "audio/midi");
      } catch (Exception e) {
        exit();
        return;
      }
      SetAction(ACTION_CONFIRM);
    }
    
    protected void stop() {
      RemoveAction(ACTION_CONFIRM);
      SetAction(COMMON_ACTIONS);
    }

    protected void stateAction() {
      if (keyUp(aYES)) {
        Sound_Enabled = true;
        try {
          midi.start();
        } catch (Exception e) {
          
          exit();
          return;
        }
      } else if (keyUp(aNO)) {
        Sound_Enabled = false;
      } else {
        return;
      }
      SetState(STATE_MENU);
    }

    protected void stateUpdate() {
      clearCanvas(0x000000);
      font3.drawString(gfx, "Enable Sound?", getWidth()/2, getHeight()/2, Graphics.HCENTER|Graphics.BOTTOM);
      drawSoftButtons(font3,"NO","YES");
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Menu State
  private class MenuStateClass extends GameState {
   private final Object mlNEW     = "NEW GAME";
   private final Object mlRESUME   = "RESUME";
   private final Object mlOPTIONS  = "OPTIONS";
   private final Object mlHELP     = "HELP";
   private final Object mlABOUT    = "ABOUT";
   private final Object mlEXIT     = "EXIT";
   private Object[] MenuList;

   protected int selected = 0;
   
    protected void start() {
      if (maze == null) {
        MenuList = new Object[] {mlNEW,mlOPTIONS,mlHELP,mlEXIT,mlABOUT};
      } else {
        MenuList = new Object[] {mlRESUME,mlNEW,mlOPTIONS,mlHELP,mlEXIT,mlABOUT};
      }
    }

    protected void stateAction() {
      if (keyUp(aUP)) {
        selected--;
        if (selected < 0) selected += MenuList.length;
      } else if (keyUp(aDOWN)) {
        selected++;
        if (selected >= MenuList.length) selected -= MenuList.length;
      } else if (keyUp(aFIRE)) {
        if (MenuList[selected] == mlRESUME) {
          SetState(STATE_PLAY);
        } else if (MenuList[selected] == mlNEW) {
          if (maze != null) { // confirm terminate current game
            // save option?
            newMaze();
          }
          SetState(STATE_PLAY);
        } else if (MenuList[selected] == mlOPTIONS) {
          SetState(new OptionStateClass());
        } else if (MenuList[selected] == mlHELP) {
          SetState(new HelpStateClass());
        } else if (MenuList[selected] == mlABOUT) {
          SetState(new AboutStateClass());
        } else if (MenuList[selected] == mlEXIT) {
          exit();
        }
      }
    }
    
    protected void stateUpdate() {
     GameFont font = null;
     String[] ml = new String[5];
     int y,ms;
      clearCanvas(0x000000);
      y = (getHeight()/2)-(font1.textHeight()*(MenuList.length-1))/2;
      for (int i=0; i < ml.length; i++) {
        ms = selected-2+i;
        if (ms < 0) ms += MenuList.length;
        if (ms >= MenuList.length) ms -= MenuList.length;
        ml[i] = (String) MenuList[ms];
      }
      for (int i=0; i < ml.length; i++) {
        font = (i != 2) ? font1 : font3;
        font.drawString(gfx,(String) ml[i],getWidth()/2,y,Graphics.HCENTER|Graphics.TOP);
        y += font.textHeight();
      }
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Option State
  private class OptionStateClass extends GameState {
   private final GameAction aSELECT = new GameAction(SOFT_RIGHT);
   private final GameAction[] ACTION_OPTIONS = new GameAction[] {aSELECT};

   private final Object mlSOUND = "Enable Sounds";
   private final Object mlMUSIC = "Enable Music";
   private final Object[] MenuList = {mlSOUND,mlMUSIC};
   private boolean[] OptList = {Sound_Enabled,Music_Enabled};
   private int selected = 0;
   
    protected void start() {
      OptList[0] = Sound_Enabled;
      OptList[1] = Music_Enabled;
      SetAction(ACTION_OPTIONS);
      gfx.setColor(0xFFFFFF);
    }
    
    protected void stop() {
      RemoveAction(ACTION_OPTIONS);
    }

    protected void stateAction() {
      if (keyUp(aUP)) {
        selected--;
        if (selected < 0) selected += MenuList.length;
      } else if (keyUp(aDOWN)) {
        selected++;
        if (selected >= MenuList.length) selected -= MenuList.length;
      } else if (keyUp(aFIRE) || keyUp(aLEFT) || keyUp(aRIGHT) || keyUp(aSELECT)) {
        if (selected == 0) {
          OptList[selected] = (OptList[selected] != true);
          Sound_Enabled = OptList[0];
        } else if (selected == 1) {
          OptList[selected] = (OptList[selected] != true);
          Music_Enabled = OptList[1];
        }
        try {
          if (midi != null) {
            if (Sound_Enabled && Music_Enabled) {
              midi.start();
            } else {
              midi.stop();
            }
          }
        } catch(Exception e) {
          exit();
          return;
        }
      } else if (keyUp(aMENU)) { // aMENU released
        SetState(STATE_MENU);
      }
    }
    
    protected void stateUpdate() {
     int th = font3.textHeight();
      clearCanvas(0x000000);
      font3.drawString(gfx, "OPTIONS", getWidth()/2, 1, Graphics.HCENTER|Graphics.TOP);
      for (int i=0; i < MenuList.length; i++) {
        if (OptList[i]) {
          gfx.fillRect(32-th, 24+i*(th+4), th+1, th+1);
        } else {
          gfx.drawRect(32-th, 24+i*(th+4), th, th);
        }
        font3.drawString(gfx, (String) MenuList[i], 36, 24+i*(th+4), Graphics.LEFT|Graphics.TOP);
        if (selected == i) {
          gfx.drawRect(32-th-2, 24+i*(th+4)-2, getWidth()-(32-th-2)*2, th+4);
        }
      }
      drawSoftButtons(font1,"BACK","SELECT");
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Help State
  private class HelpStateClass extends GameState {
    protected void stateAction() {
      if (keyUp(aMENU)) { // aMENU released
        SetState(STATE_MENU);
      }
    }
    
    protected void stateUpdate() {
      clearCanvas(0x000000);
      font3.drawString(gfx, "HELP", getWidth()/2, 1, Graphics.HCENTER|Graphics.TOP);
      drawSoftButtons(font1,"BACK",null);
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // About State
  private class AboutStateClass extends GameState {
    protected void stateAction() {
      if (keyUp(aMENU)) { // aMENU released
        SetState(STATE_MENU);
      }
    }
    
    protected void stateUpdate() {
      clearCanvas(0x000000);
      font3.drawString(gfx, "ABOUT", getWidth()/2, 1, Graphics.HCENTER|Graphics.TOP);
      drawSoftButtons(font1,"BACK",null);
      flushGraphics();
    }
  }

  //-----------------------------------------------------------------------//
  // Run State
  protected class RunStateClass extends GameState {
   private final GameAction aMAP  = new GameAction(SOFT_RIGHT);
//#if DefaultConfiguration
//#    private final GameAction aDEBUG = new GameAction(KEY_POUND);
//#else
   private final GameAction aDEBUG = new GameAction(KEY_NUM9);
//#endif
   private final GameAction[] ACTION_PLAY = new GameAction[] {aMAP,aDEBUG};

   private Runtime rt = Runtime.getRuntime();
   private long tMem=0,fMem=0,tick=0;

   protected boolean paused = false;
   
    protected void start() {
      SetAction(ACTION_PLAY);
      paused = false;
      if (maze == null) {
        Runtime.getRuntime().gc();
        try {
          tsMAP = Image.createImage("/img/tset_maze_map.png");
          tsGround = Image.createImage("/img/ground.png");
          tsFringe = Image.createImage("/img/fringe.png");
          tsItems = Image.createImage("/img/items.png");
          tsPC = Image.createImage("/img/char1.png");
          pc = new GameSprite(tsPC,16,24);
          pc.setRefPixelPosition(0, 16);
          pc.setCollisionRect(2, 16, pc.getWidth()-4, 8);
          // walk up,down,left,right
          pc.setAnimateMove(new int[] {0, 1, 2, 1} ,new int[] {6, 7, 8, 7} ,new int[] {9, 10, 11, 10}, new int[] {3, 4, 5, 4});
          // stand up,down,left,right
          pc.setAnimateIdle(new int[] {1}, new int[] {7}, new int[] {10}, new int[] {4});
          pc.setIdle();
          tsMOB_Skeleton = Image.createImage("/img/skeleton.png");
          mobSkeleton = new GameSprite(tsMOB_Skeleton,20,24);
          mobSkeleton.setRefPixelPosition(0, 16);
          mobSkeleton.setCollisionRect(2, 16, mobSkeleton.getWidth()-4, 8);
        } catch(Exception e) {
          e.printStackTrace();
          exit();
        }
        System.gc();
        if (SEED_VALUE != 0) rand.setSeed(SEED_VALUE);
        pc.start();
        newMaze();
        System.gc();
      }
    }

    protected void stop() {
      RemoveAction(ACTION_PLAY);
      paused = true;
    }

    protected void stateAction() {
      if (keyUp(aMENU)) { // aMENU released
        STATE_MENU.selected = 0;
        SetState(STATE_MENU);
      } else if (keyUp(aMAP)) { // soft right
        map_mode++;
        if (map_mode == 1) {
          try {
            tsHUD = Image.createImage("/img/tset_maze_hud12.png");
          } catch (IOException ex) {
            ex.printStackTrace();
          }
          spriteHUD = new Sprite(tsHUD,tsHUD.getWidth()/15,tsHUD.getWidth()/15);
        } else {
          tsHUD = null;
          spriteHUD = null;
          map_mode = 0;
        }
      } else if (keyUp(aDEBUG)) {
        if (++debug_flag > 4) debug_flag = 0;
      } else {
        checkKeys();
      }
    }
    
    private void checkKeys() {
     double dx=0.0,dy=0.0;
      if (!paused && (map_mode == 0)) {
        if (keyDown(aUP)) {
          pc.setMove(UP);
          dy = -pc.SPEED*fpsScale;
        } else if (keyDown(aDOWN)) {
          pc.setMove(DOWN);
          dy = pc.SPEED*fpsScale;
        } else if (keyDown(aLEFT)) {
          pc.setMove(LEFT);
          dx = -pc.SPEED*fpsScale;
        } else if (keyDown(aRIGHT)) {
          pc.setMove(RIGHT);
          dx = pc.SPEED*fpsScale;
        } else {
          pc.setIdle();
          if (keyUp(aFIRE)) {
           int w,h,c,r;
           int[] collide = pc.getCollisionRect();
           int[] rect = new int[4];
            w = room.tlBase.getCellWidth();
            h = room.tlBase.getCellHeight();
            rect[0] = pc.getX()-room.getX() + collide[0] + (collide[2]/2)-1; // sprite axis
            rect[1] = pc.getY()-room.getY() + collide[1] + (collide[3]/2)-1;  // sprite axis
            rect[0] /= w;
            rect[1] /= h;
            for (r = rect[1]-1; r <= rect[1]+1; r++) {
              for (c = rect[0]-1; c <= rect[0]+1; c++) {
                if (
                      (
                          ((pc.dir == LEFT)  && (c == rect[0]-1))
                       || ((pc.dir == RIGHT) && (c == rect[0]+1))
                       || ((pc.dir == UP)    && (r == rect[1]-1))
                       || ((pc.dir == DOWN)  && (r == rect[1]+1))
                       || ((c == rect[0]) && (r == rect[1])) // tile the pc is standing on
                      )
                   && (room.tlItems.getCell(c,r) < 0)
                   ) { // found something
                  action_item(c,r);
                  return;
                }
              }
            }
          }
        }
        movePC(dx,dy);
      }
    }
    
    protected void stateUpdate() {
      if (first_pass) return;
      clearCanvas(0x000000);
      if (map_mode != 0) {
        drawHUD();
      } else {
        room.paint(gfx,0,0);
      }
      gfx.setColor(0xFFFFFF);
      font1.drawString(gfx,Long.toString((long) fps),1,2,Graphics.LEFT|Graphics.TOP);
      int y = 10;
      for (int i=0; i < txtGameInfo.size(); i++) {
        y += drawWrapped(font3, gfx, (String) txtGameInfo.elementAt(i),10,y, getWidth()-20 ,Graphics.LEFT|Graphics.TOP);
      }
      int i = 1;
      if (debug_flag == 1) {
        font1.drawString(gfx,(tMem - fMem)+"/"+tMem+" ("+fMem+" free)",1,getHeight()-10*i++,Graphics.LEFT|Graphics.BOTTOM);
        font1.drawString(gfx,"seed = "+maze.SEED_VALUE, 1, getHeight()-10*i++, Graphics.LEFT|Graphics.BOTTOM);
        font1.drawString(gfx,"Map["+maze.cellCurrent.y+"]["+maze.cellCurrent.x+"]",1,getHeight()-10*i++,Graphics.LEFT|Graphics.BOTTOM);
      }
      drawSoftButtons(font1,"MENU","MAP");
      flushGraphics();
      tick++;
      if (tick > 100) {
        tick = 0;
        rt.gc();
        tMem = rt.totalMemory();
        fMem = rt.freeMemory();
      }
    }
  }

  /**
   * Copy an int[] by value (deep copy).
   * Avoids use of Object.clone() method (copy by ref / shallow copy)
   */
  private static int[] copyArray(int[] src) {
   int[] dst = new int[src.length];
    System.arraycopy(src, 0, dst, 0, src.length);
    return dst;
  }

  private static Object[] copyArray(Object[] src) {
   Object[] dst = new Object[src.length];
    for (int i=0; i < dst.length; i++) {
      if (dst[i] != null) dst[i] = copyArray((int[]) src[i]);
    }
    return dst;
  }
  
  private class GameSprite extends Sprite implements Runnable {
   private final int MAX_DIR = Math.max(Math.max(Math.max(UP,DOWN),LEFT),RIGHT)+1;
   private double SPEED = 3.0*(30.0/FPS); // guage at 30 fps
   private Image tset = null;
   private double x=0,y=0;
   private boolean idle = false; // walking or standing?
   private int dir = 0;
   private int delay = 0; // runtime variable, set from aniMoveDelay or aniIdleDelay
   private int[] collide = new int[4];
   private Object[] aniMove = new Object[MAX_DIR];
   private Object[] aniIdle = new Object[MAX_DIR];
   private int[] aniMoveDelay = new int[MAX_DIR]; // framespeed in milliseconds
   private int[] aniIdleDelay = new int[MAX_DIR]; // framespeed in milliseconds
   private Vector inventory = new Vector();
   private Thread thread;

    public GameSprite(Image tset) {
      this(tset,tset.getWidth(),tset.getHeight());
    }

    public GameSprite(Image tset, int w, int h) {
      super(tset,w,h);
      aniMove[UP] = new int[] {1};  aniMove[DOWN] = new int[] {2};  aniMove[LEFT] = new int[] {3};  aniMove[RIGHT] = new int[] {4};
      aniIdle[UP] = new int[] {1};  aniIdle[DOWN] = new int[] {2};  aniIdle[LEFT] = new int[] {3};  aniIdle[RIGHT] = new int[] {4};
      aniMoveDelay[UP] = 200;       aniMoveDelay[DOWN] = 200;       aniMoveDelay[LEFT] = 200;       aniMoveDelay[RIGHT] = 200;
      aniIdleDelay[UP] = 200;       aniIdleDelay[DOWN] = 200;       aniIdleDelay[LEFT] = 200;       aniIdleDelay[RIGHT] = 200;
      collide = new int[] {0,0,w,h};
    }
      
    public GameSprite(GameSprite s) {
      super(s);
      this.tset = s.tset;
      this.x = s.x;
      this.y = s.y;
      this.idle = s.idle;
      this.dir = s.dir;
      this.delay = s.delay;
      this.collide = copyArray(s.collide); // int[]
      this.aniMove = copyArray(s.aniMove); // Object[(int[])]
      this.aniIdle = copyArray(s.aniIdle); // Object[(int[])]
      this.aniMoveDelay = copyArray(s.aniMoveDelay); // int[]
      this.aniIdleDelay = copyArray(s.aniIdleDelay); // int[]
//        this.inventory = s.inventory; // copy?
//      this.thread = s.thread; // do not copy!
    }
    
    private void start() {
      thread = new Thread(this);
      thread.start();
    }

    private void setAnimateMove(int[] up, int[] down, int[] left, int[] right) {
      aniMove[UP] = up; aniMove[DOWN] = down; aniMove[LEFT] = left; aniMove[RIGHT] = right;
    }

    private void setAnimateMoveDelay(int up, int down, int left, int right) {
      aniMoveDelay[UP] = up; aniMoveDelay[DOWN] = down; aniMoveDelay[LEFT] = left; aniMoveDelay[RIGHT] = right;
    }

    private void setAnimateIdle(int[] up, int[] down, int[] left, int[] right) {
      aniIdle[UP] = up; aniIdle[DOWN] = down; aniIdle[LEFT] = left; aniIdle[RIGHT] = right;
    }

    private void setAnimateIdleDelay(int up, int down, int left, int right) {
      aniIdleDelay[UP] = up; aniIdleDelay[DOWN] = down; aniIdleDelay[LEFT] = left; aniIdleDelay[RIGHT] = right;
    }

    private void setMove(int d) {
      if (idle || (d != dir)) {
        dir = d;
        setFrameSequence((int []) aniMove[dir]);
        delay = aniMoveDelay[dir];
        idle = false;
      }
    }

    private void setIdle() {
      if (!idle) {
        if (dir == 0) dir = DOWN;
        setFrameSequence((int[]) aniIdle[dir]);
        delay = aniIdleDelay[dir];
        idle = true;
      }
    }

    public void setPosition(double x, double y) {
      this.x = x;
      this.y = y;
      super.setPosition((int) this.x, (int) this.y);
    }
    
    public void setPosition(int x, int y) {
      setPosition((double) x, (double) y);
    }

    public void setCollisionRect(int x, int y, int w, int h) {
      collide = new int[] {x,y,w,h};
      defineCollisionRectangle(x,y,w,h);
    }

    public int[] getCollisionRect() {
      return collide;
    }

    public void move(double dx, double dy) {
      setPosition(this.x+dx,this.y+dy);
    }
    
    public void move(int x, int y) {
      move((double) x, (double) y);
    }

    public void run() {
      while (running) {
        if (!STATE_PLAY.paused) {
          nextFrame();
        }
        try {
          Thread.sleep(delay);
        } catch(Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
}
