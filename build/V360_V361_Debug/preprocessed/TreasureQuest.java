import javax.microedition.lcdui.Display;
import javax.microedition.midlet.*;

public class TreasureQuest extends MIDlet {
 private Display display = null;
 private Engine game = null;
 
  protected void startApp() throws MIDletStateChangeException {
    if (game == null) {
      display = Display.getDisplay(this);
      try {
        game = new Engine(this,false);
        game.setFullScreenMode(true);
        display.setCurrent(game);
        game.start();
      } catch(Exception e) {
        destroyApp(true);
      }
    }
  }

  protected void pauseApp() {
    if (game.currentState == game.STATE_PLAY) game.STATE_PLAY.paused = true;
  }

  protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    display = null;
    game = null;
    System.gc();
    notifyDestroyed();
  }
}
