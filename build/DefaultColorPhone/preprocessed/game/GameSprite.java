/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import java.util.Vector;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import main.Engine;

/**
 *
 * @author Chris
 */
public class GameSprite extends Sprite implements Runnable {
 private final int MAX_DIR = Math.max(Math.max(Math.max(Engine.UP,Engine.DOWN),Engine.LEFT),Engine.RIGHT)+1;
 public static final double SPEED = 1.5*(30.0/Engine.FPS); // guage at 30 fps
 private Image tset = null;
 private double x=0,y=0;
 public boolean idle = false; // walking or standing?
 public int dir = 0;
 private int delay = 0; // runtime variable, set from aniMoveDelay or aniIdleDelay
 private int[] collide = new int[4];
 private Object[] aniMove = new Object[MAX_DIR];
 private Object[] aniIdle = new Object[MAX_DIR];
 private int[] aniMoveDelay = new int[MAX_DIR]; // framespeed in milliseconds
 private int[] aniIdleDelay = new int[MAX_DIR]; // framespeed in milliseconds
 public Vector inventory = new Vector();
 private Thread thread;
 private Engine engine;

  public GameSprite(Engine engine, Image tset) {
    this(engine, tset,tset.getWidth(),tset.getHeight());
  }

  public GameSprite(Engine engine, Image tset, int w, int h) {
    super(tset,w,h);
    this.engine = engine;
    aniMove[Engine.UP] = new int[] {1};  aniMove[Engine.DOWN] = new int[] {2};  aniMove[Engine.LEFT] = new int[] {3};  aniMove[Engine.RIGHT] = new int[] {4};
    aniIdle[Engine.UP] = new int[] {1};  aniIdle[Engine.DOWN] = new int[] {2};  aniIdle[Engine.LEFT] = new int[] {3};  aniIdle[Engine.RIGHT] = new int[] {4};
    aniMoveDelay[Engine.UP] = 200;       aniMoveDelay[Engine.DOWN] = 200;       aniMoveDelay[Engine.LEFT] = 200;       aniMoveDelay[Engine.RIGHT] = 200;
    aniIdleDelay[Engine.UP] = 200;       aniIdleDelay[Engine.DOWN] = 200;       aniIdleDelay[Engine.LEFT] = 200;       aniIdleDelay[Engine.RIGHT] = 200;
    collide = new int[] {0,0,w,h};
  }

  public GameSprite(Engine engine, GameSprite s) {
    super(s);
    this.engine = engine;
    this.tset = s.tset;
    this.x = s.x;
    this.y = s.y;
    this.idle = s.idle;
    this.dir = s.dir;
    this.delay = s.delay;
    this.collide = Engine.copyArray(s.collide); // int[]
    this.aniMove = Engine.copyArray(s.aniMove); // Object[(int[])]
    this.aniIdle = Engine.copyArray(s.aniIdle); // Object[(int[])]
    this.aniMoveDelay = Engine.copyArray(s.aniMoveDelay); // int[]
    this.aniIdleDelay = Engine.copyArray(s.aniIdleDelay); // int[]
//        this.inventory = s.inventory; // copy?
//      this.thread = s.thread; // do not copy!
  }

  public void start() {
    thread = new Thread(this);
    thread.start();
  }

  public void setAnimateMove(int[] up, int[] down, int[] left, int[] right) {
    aniMove[Engine.UP] = up; aniMove[Engine.DOWN] = down; aniMove[Engine.LEFT] = left; aniMove[Engine.RIGHT] = right;
  }

  public void setAnimateMoveDelay(int up, int down, int left, int right) {
    aniMoveDelay[Engine.UP] = up; aniMoveDelay[Engine.DOWN] = down; aniMoveDelay[Engine.LEFT] = left; aniMoveDelay[Engine.RIGHT] = right;
  }

  public void setAnimateIdle(int[] up, int[] down, int[] left, int[] right) {
    aniIdle[Engine.UP] = up; aniIdle[Engine.DOWN] = down; aniIdle[Engine.LEFT] = left; aniIdle[Engine.RIGHT] = right;
  }

  public void setAnimateIdleDelay(int up, int down, int left, int right) {
    aniIdleDelay[Engine.UP] = up; aniIdleDelay[Engine.DOWN] = down; aniIdleDelay[Engine.LEFT] = left; aniIdleDelay[Engine.RIGHT] = right;
  }

  public void setMove(int d) {
    if (idle || (d != dir)) {
      dir = d;
      setFrameSequence((int []) aniMove[dir]);
      delay = aniMoveDelay[dir];
      idle = false;
    }
  }

  public void setIdle() {
    if (!idle) {
      if (dir == 0) dir = Engine.DOWN;
      setFrameSequence((int[]) aniIdle[dir]);
      delay = aniIdleDelay[dir];
      idle = true;
    }
  }

  public void setPosition(double x, double y) {
    this.x = x;
    this.y = y;
    super.setPosition((int) this.x, (int) this.y);
  }

  public void setPosition(int x, int y) {
    setPosition((double) x, (double) y);
  }

  public void setCollisionRect(int x, int y, int w, int h) {
    collide = new int[] {x,y,w,h};
    defineCollisionRectangle(x,y,w,h);
  }

  public int[] getCollisionRect() {
    return collide;
  }

  public void move(double dx, double dy) {
    setPosition(this.x+dx,this.y+dy);
  }

  public void move(int x, int y) {
    move((double) x, (double) y);
  }

  public void run() {
    while (engine.running) {
      if (!engine.STATE_PLAY.paused) {
        nextFrame();
      }
      try {
        Thread.sleep(delay);
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
}
