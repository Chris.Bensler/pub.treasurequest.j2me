package game;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Enumeration;
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;

/**
 *
 * @author Chris
 */
public abstract class GameClass extends GameCanvas implements Runnable {
 
//#if DefaultConfiguration
//#  public static final int SOFT_LEFT  = -21;
//#  public static final int SOFT_RIGHT = -22;
//#  public static final int SOFT_MENU  = -23;
//#else
 public static final int SOFT_LEFT  = KEY_STAR;
 public static final int SOFT_RIGHT = KEY_POUND;
 public static final int SOFT_MENU  = KEY_NUM0;
//#endif
 
 protected boolean running = false;

 private int FPS_MAX = 30;
 private int FPS_MIN = 8;
 private int fpsLimit = FPS_MAX;
 private double frameSpeed = 1000.0/fpsLimit;
 public double fpsScale = 1.0;
 private long frameTime = 0;
 private long fStart,fStop,fTime;
 private int frameCount = 0;
 public double fps  = fpsLimit;

 public Graphics gfx = null;
// private int width = 0;
// private int height = 0;

 private Hashtable actionTable = null;
 public GameState currentState = null;

 private Thread runtime = null;
 
  protected GameClass(boolean supress) {
    super(supress);
    gfx = getGraphics();
//    width = getWidth();
//    height = getHeight();
    actionTable = new Hashtable();
  }
  
  protected void start() {
    running = true;
    runtime = new Thread(this);
    runtime.start();
  }
  
  protected final void stop() {
    running = false;
    runtime = null;
    gfx = null;
  }
  
//  public void sizeChanged(int w, int h) {
//    width = w;
//    height = h;
//  }

  protected final void keyPressed(int key) {
   GameAction action = GetAction(key);
    if (action != null) {
      action.count = 1;
    }
  }

  protected final void keyReleased(int key) {
   GameAction action = GetAction(key);
    if (action != null) {
      action.count = -1;
    }
  }
  
  // returns true if the key has just changed from the up to the down state
  public static final boolean keyPress(GameAction action) {
    return (action.count == 1);
  }
  
  public static final boolean keyDown(GameAction action) {
    return (action.count > 0);
  }
  
  public static final boolean keyUp(GameAction action) {
    return (action.count == -1);
  }

  private final void stateAction() {
   Enumeration e;
   GameAction action;
   boolean flag = false;
    e = actionTable.keys();
    while (e.hasMoreElements()) {
      action = (GameAction) actionTable.get((Integer) e.nextElement());
      if (action.count != 0) {
        if ((currentState != null) && !flag) currentState.stateAction();
        flag = true;
        action.count++;
      }
    }
  }

  private final void stateUpdate() {
    if (currentState != null) currentState.stateUpdate();
  }
  
  public final void run() {
    frameTime = System.currentTimeMillis();
    while (running) {
      fStart = System.currentTimeMillis();
      stateAction();
      stateUpdate();
      fStop = System.currentTimeMillis()-fStart;
      if (fStop < frameSpeed) { // too fast
        try { Thread.sleep((long) frameSpeed - fStop); } catch(Exception e) { }
        if (fpsLimit < FPS_MAX) {
          fpsLimit++;
          frameSpeed = 1000.0/fpsLimit;
        }
      } else { // too slow
        if (fpsLimit > FPS_MIN) {
          fpsLimit--;
          frameSpeed = 1000.0/fpsLimit;
        }
      }
      frameCount++;
      fTime = System.currentTimeMillis()-frameTime;
      if (fTime >= 1000.0) {
        fps = frameCount*(1000.0/fTime);
        fpsScale = FPS_MAX/fps;
        frameCount = 0;
        frameTime = System.currentTimeMillis();
      }
    }
  }

  protected final void setFPS_MIN(int min) {
    FPS_MIN = min;
    if (fpsLimit < min) fpsLimit = min;
    frameSpeed = 1000.0/fpsLimit;
  }
  
  protected final void setFPS_MAX(int max) {
    FPS_MAX = max;
    if (fpsLimit > max) fpsLimit = max;
    frameSpeed = 1000.0/(long) fpsLimit;
  }

  public final void SetState(GameState state) {
    if (currentState != null) currentState.stop();
    currentState = state;
    if (currentState != null) currentState.start();
  }
  
//  private GameState GetState() {
//    return currentState;
//  }

  private final void SetAction(GameAction action) {
    actionTable.put(new Integer(action.key), action);
  }

  public final void SetAction(GameAction[] action) {
    for (int i=0; i < action.length; i++) {
      SetAction(action[i]);
    }
  }

  private final GameAction GetAction(int key) {
    return (GameAction) actionTable.get(new Integer(key));
  }

//  private final GameAction[] GetAction(int[] key) {
//   GameAction[] action;
//    action = new GameAction[key.length];
//    for (int i=0; i < key.length; i++) {
//      action[i] = GetAction(key[i]);
//    }
//    return action;
//  }

  private final void RemoveAction(GameAction action) {
    actionTable.remove(new Integer(action.key));
  }

  public final void RemoveAction(GameAction[] action) {
    for (int i=0; i < action.length; i++) {
      RemoveAction(action[i]);
    }
  }
}
