/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.maze;

/**
 *
 * @author Chris
 */
  public class MazeCell {
   public boolean visited = false;
   public boolean visible = false;
   protected int gap = 0;
   public int x = 0;
   public int y = 0;
   public int count = 0;
   public int score = 0;
   protected int keys = 0; // 8 bits, 1 bit per key
   protected int framex = 0;
   protected int framey = 0;
   protected int frame = 0;
   // a bitflag for each random item drop (chests/potions/coins/keys/mobs) in a given room (max 32)
   // bit is on if the item has been opened/taken
   public int items = 0;
   // which door in this cell is locked and what key belongs to that door
   // lock & 0xF = LEFT|RIGHT|UP|DOWN toggle bits
   // (lock >> (8+4*(LEFT >> 1))) & 0xF = left key
   public int lock = 0;
  }
