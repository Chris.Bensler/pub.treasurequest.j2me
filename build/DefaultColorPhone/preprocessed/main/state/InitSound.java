/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.state;

import java.io.InputStream;
import javax.microedition.lcdui.Graphics;
import javax.microedition.media.Manager;
import main.Engine;
import game.GameAction;
import game.GameClass;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class InitSound extends GameState {
 private final GameAction aNO = new GameAction(GameClass.SOFT_LEFT);
 private final GameAction aYES = new GameAction(GameClass.SOFT_RIGHT);
 private final GameAction[] ACTION_CONFIRM = new GameAction[] {aNO,aYES};

  public InitSound(Engine engine){
    super(engine);
    try {
      InputStream is = getClass().getResourceAsStream("/snd/01.mid");
      engine.midi = Manager.createPlayer(is, "audio/midi");
    } catch (Exception e) {
      engine.exit();
      return;
    }
    engine.SetAction(ACTION_CONFIRM);
  }

  protected final void stop() {
    engine.RemoveAction(ACTION_CONFIRM);
    engine.SetAction(engine.COMMON_ACTIONS);
  }

  protected final void stateAction() {
    if (Engine.keyUp(aYES)) {
      engine.Sound_Enabled = true;
      try {
        engine.midi.start();
      } catch (Exception e) {
        engine.exit();
        return;
      }
    } else if (Engine.keyUp(aNO)) {
      engine.Sound_Enabled = false;
    } else {
      return;
    }
    engine.SetState(engine.STATE_MENU);
  }

  protected final void stateUpdate() {
    engine.clearCanvas(0x000000);
    engine.font3.drawString(engine.gfx, "Enable Sound?", engine.getWidth()/2, engine.getHeight()/2, Graphics.HCENTER|Graphics.BOTTOM);
    engine.drawSoftButtons(engine.font3,"NO","YES");
    engine.flushGraphics();
  }
}
