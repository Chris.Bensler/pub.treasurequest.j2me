/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.state;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import main.Engine;
import game.GameState;

/**
 *
 * @author Chris
 */
public final class Splash extends GameState {
 private final Timer timer = new Timer();
 private Image splash = null;
 
  protected Splash(Engine engine) {
    super(engine);
    try {
      splash = Image.createImage("/img/treasurequest.png");
    } catch (IOException e) {
      System.out.println("Failed to load splash");
    }
    timer.schedule(new SplashTimer(), Engine.SPLASH_TIMEOUT);
  }

  protected final void stop() {
    Runtime.getRuntime().gc();
  }

  private final class SplashTimer extends TimerTask {
    public void run() {
      engine.SetState(new InitSound(engine));
    }
  }

  protected final void stateAction() {}
  protected final void stateUpdate() {
    engine.clearCanvas(0x000000);
    engine.gfx.drawImage(splash, engine.getWidth()/2, engine.getHeight()/2, Graphics.HCENTER|Graphics.VCENTER);
    engine.flushGraphics();
  }
}
