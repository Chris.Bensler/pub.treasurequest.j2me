package main;

import game.GameFont;
import game.GameClass;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.media.Player;
import game.GameAction;
import game.GameSprite;
import main.maze.MazeKey;
import main.maze.MazeMap;
import main.maze.MazeRoom;

public class Engine extends GameClass {
 private TreasureQuest parent = null;
 public GameFont font1 = null;
// public GameFont font2 = null;
 public GameFont font3 = null;
 public Player midi = null;
 public boolean Sound_Enabled = false;
 public boolean Music_Enabled = true;

 public static final int FPS = 30;
 public static final int LOGO_TIMEOUT   = 250;
 public static final int SPLASH_TIMEOUT = 5000;

// final LogoStateClass     STATE_LOGO    = new LogoStateClass();
// private SplashStateClass   STATE_SPLASH  = new SplashStateClass();
// final SndInitStateClass  STATE_SNDINIT = new SndInitStateClass();
 public main.state.Menu     STATE_MENU    = new main.state.Menu(this);
// final OptionStateClass   STATE_OPTIONS = new OptionStateClass();
// final HelpStateClass     STATE_HELP    = new HelpStateClass();
// final AboutStateClass    STATE_ABOUT   = new AboutStateClass();
 public main.state.Play     STATE_PLAY    = new main.state.Play(this);

 // common actions
 public final GameAction aUP    = new GameAction(getKeyCode(UP));
 public final GameAction aDOWN  = new GameAction(getKeyCode(DOWN));
 public final GameAction aLEFT  = new GameAction(getKeyCode(LEFT));
 public final GameAction aRIGHT = new GameAction(getKeyCode(RIGHT));
 public final GameAction aFIRE  = new GameAction(getKeyCode(FIRE));
 public final GameAction aMENU  = new GameAction(SOFT_LEFT);

 public final GameAction[] COMMON_ACTIONS = new GameAction[] {aUP,aDOWN,aLEFT,aRIGHT,aFIRE,aMENU};
 
 private static final int MAX_MAZEX = 14;
 private static final int MAX_MAZEY = 14;
 // RATIOS are int values in the range of 0 to 100 representing a percentage chance of that behaviour
 private static final int DIR_RATIO = 60; // controls the tendency to continue going the same direction
 private static final int VISIT_RATIO = 40; // controls the tendency to visit neighbouring cells, setting this too low will produce small and unusable mazes
 private static final int FILL_RATIO = 60; // controls how much of the specified region it will try to fill in
 private static final int GAP_RATIO = 30; // controls the tendency to create extra gaps (doorways)
 private static final int LOCK_RATIO = 100; // controls the tendency to lock a door (max of 8 locked doors per maze)
 public static final int SEED_VALUE = 2051716863; // any value other than 0 will preset the RNG for the maze
 private static final int MAX_REDO = 10; // number of tries to maximize the fill ratio
 private static final int MIN_AREA = Math.max(MAX_MAZEX, MAX_MAZEY)+1; // minimum number of valid cells

 public MazeMap maze = null;
 public MazeRoom room = null;

 private int viewX=0,viewY=0,viewW,viewH;
 public Image tsGround,tsFringe,tsItems,tsMAP,tsHUD,tsPC,tsMOB_Skeleton;
 public Sprite spriteMAP,spriteHUD;
 public GameSprite pc,mobSkeleton;
 public Random rand = new Random();
 private Timer timer = null;

 public boolean first_pass = true;

 public Vector txtGameInfo = new Vector();
 public int debug_flag = 1;
 public int map_mode = 0;

  protected Engine(TreasureQuest parent, boolean supress) throws Exception {
    super(supress);
    this.parent = parent;
    setFPS_MAX(FPS);
    setFPS_MIN(1);
    font1 = new GameFont("/font/04.png",0xFFFFFF);
//    font2 = new GameFont("/font/02.png",0xFFFFFF);
    font3 = new GameFont("/font/03.png",0xFFFFFF);
    viewW = getWidth();
    viewH = getHeight();
    SetAction(COMMON_ACTIONS);
    SetState(new main.state.Logo(this));
  }
  
  protected final void sizeChanged(int w, int h) {
    super.sizeChanged(w, h);
    viewW = w;
    viewH = h;
  }
  
  public final void start() {
    super.start();
    timer = new Timer();
  }
  
  public final void exit() {
    stop();
    tsMAP = null;
    tsHUD = null;
    spriteMAP = null;
    spriteHUD = null;
    parent.notifyDestroyed();
  }

  public final void clearCanvas(int color) {
   int old_color = gfx.getColor();
    gfx.setColor(color);
    gfx.fillRect(0,0,getWidth(),getHeight());
    gfx.setColor(old_color);
  }
  
  public final void drawSoftButtons(GameFont font, String left, String right) {
   int x,y;
    x = 16;
    y = getHeight()-4;
    if (left != null) {
      font.drawString(gfx, left,  x, y, Graphics.HCENTER|Graphics.BOTTOM);
      gfx.fillTriangle(x-2,y,x+2,y,x,y+2);                   // soft left arrow
    }
    if (right != null) {
      x = getWidth()-x;
      font.drawString(gfx, right, x, y, Graphics.HCENTER|Graphics.BOTTOM);
      gfx.fillTriangle(x-2,y,x+2,y,x,y+2);                   // soft right arrow
    }
  }

  public static final int drawWrapped(GameFont font, Graphics gfx, String txt, int x, int y, int w, int align) {
   int start = 0, stop = 0, iy = y, ww = 0;
   char c;
    align = (align & (Graphics.LEFT|Graphics.RIGHT)) | Graphics.TOP; // force top alignment, retain specified horizontal alignment
    for (int i=0; i < txt.length(); i++) {
      c = txt.charAt(i);
      ww += font.textWidth(c);
      if (c <= ' ') stop = i;
      if ((ww >= w) || (c == '\n')) {
        if (stop == start) stop = i;
        font.drawString(gfx,txt.substring(start, stop), x, (iy += font.textHeight()), align);
        if ((c == '\n') || (stop != i)) stop++;
        start = stop;
        ww = 0;
        if (start <= i) {
          ww = font.textWidth(txt.substring(start, i));
        }
      }
    }
    if (start != txt.length()) font.drawString(gfx,txt.substring(start, txt.length()), x, (iy += font.textHeight()), align);
    return iy;
  }

  public final void newMaze() {
   MazeMap mazeRedo;
   int redo = MAX_REDO;
   int x,y;
    spriteMAP = new Sprite(tsMAP,tsMAP.getWidth()/15,tsMAP.getWidth()/15);
    maze = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,(first_pass && (SEED_VALUE != 0)) ? SEED_VALUE : rand.nextInt());
    while (((redo > 0) || (maze.mazeArea == 1)) && ((MIN_AREA > maze.mazeArea) || (maze.mazeArea < (FILL_RATIO/100 * maze.XMAX * maze.YMAX)))) {
      redo--;
      mazeRedo = new MazeMap(spriteMAP,MAX_MAZEX,MAX_MAZEY,DIR_RATIO,GAP_RATIO,VISIT_RATIO,FILL_RATIO,LOCK_RATIO,rand.nextInt());
      if (maze.mazeArea < mazeRedo.mazeArea) {
        maze = null;
        System.gc();
        maze = mazeRedo;
      }
      mazeRedo = null;
      System.gc();
    }
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    // place the pc
    while (true) {
      x = rand.nextInt(room.tlBase.getColumns());
      y = rand.nextInt(room.tlBase.getRows());
      if ((room.tlBase.getCell(x, y) > 1) && (room.tlFringe.getCell(x,y) == 0) && (room.tlItems.getCell(x, y) == 0)) {
        x *= room.tlBase.getCellWidth();
        y *= room.tlBase.getCellHeight();
        pc.setPosition(x,y+room.tlBase.getCellHeight()-pc.getHeight());
        break;
      }
    }
    room.setViewPort(viewX+pc.getX()+pc.getWidth()/2, viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    first_pass = false;
    spriteMAP = null;
    System.gc();
  }

  public final void drawHUD() {
   int ox,oy,x,y,w,h;
   MazeKey key;
    w = spriteHUD.getWidth();
    h = spriteHUD.getHeight();
    ox = (getWidth()-maze.XMAX*w)/2;
    oy = (getHeight()-maze.YMAX*h)/2;
    maze.draw(gfx,spriteHUD,ox,oy, debug_flag);

    // mark start room
    gfx.setColor(0xFFFF00);
    gfx.drawRect(ox+maze.cellStart.x*w-1, oy+maze.cellStart.y*h-1, w+1, h+1);

    // mark end room
    gfx.setColor(0x0000FF);
    gfx.drawRect(ox+maze.cellFinish.x*w-1, oy+maze.cellFinish.y*h-1, w+1, h+1);

    // mark current room
    gfx.setColor(0xFFFFFF);
    gfx.drawRect(ox+maze.cellCurrent.x*w-1, oy+maze.cellCurrent.y*h-1, w+1, h+1);

    for (y=0; y < maze.YMAX; y++) {
      for (x=0; x < maze.XMAX; x++) {
        if (maze.Map[y][x].visited) {
          if (debug_flag == 2) {
            gfx.drawString(""+maze.Map[y][x].count, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 3) {
            gfx.drawString(""+maze.Map[y][x].score, ox+x*w-1, oy+y*h-1, 0);
          } else if (debug_flag == 4) {
            gfx.drawString(""+(maze.Map[y][x].count*maze.Map[y][x].score), ox+x*w-1, oy+y*h-1, 0);
          }
        }
      }
    }

    // mark each door and each key
    for (int i=0; i < maze.keys.size(); i++) {
      gfx.setColor(0xFFFF00);
      key = (MazeKey) maze.keys.elementAt(i);
      if ((debug_flag != 0) || key.drop.visible) {
        x = ox + key.drop.x*w - 1;
        y = oy + key.drop.y*h - 1;
        gfx.drawRect(x+w/2, y+h/2, 1, 1);
      }
      if ((debug_flag != 0) || key.cell1.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell1.x*w - 1;
        y = oy + key.cell1.y*h - 1;
        if ((key.lock1 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock1 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock1 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
      if ((debug_flag >= 2) || key.cell2.visible) {
        gfx.setColor(0x00FF00);
        x = ox + key.cell2.x*w - 1;
        y = oy + key.cell2.y*h - 1;
        if ((key.lock2 & MazeMap.LEFT)  == MazeMap.LEFT)  gfx.drawRect(x,       y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.RIGHT) == MazeMap.RIGHT) gfx.drawRect(x+w-1,   y+h/2, 1, 1);
        if ((key.lock2 & MazeMap.UP)    == MazeMap.UP)    gfx.drawRect(x+w/2,   y, 1, 1);
        if ((key.lock2 & MazeMap.DOWN)  == MazeMap.DOWN)  gfx.drawRect(x+w/2,   y+h-1, 1, 1);
      }
    }
  }

  private class GameInfoTimeout extends TimerTask {
    public void run() {
      txtGameInfo.removeAllElements();
    }
  }

  private final class DoorLockedTimeout extends GameInfoTimeout {
    public void run() {
      super.run();
      door_locked_wait = false;
    }
  }

  private final class AnimateItem implements Runnable {
   private int item,tile,last;
   private MazeKey key;
    public AnimateItem(int item) {
      this.item = -(item+1);
      this.tile = room.tlItems.getAnimatedTile(this.item);
      if (this.tile <= 7*3) { // chest
        this.last = this.tile+2;
        run();
      } else {
        if (this.tile >= 25) { // a key
          // add to player inventory
          key = (MazeKey) maze.keys.elementAt(tile-25);
          pc.inventory.addElement(key);
          txtGameInfo.addElement("You found a "+key.description+key.name+" key.\n"
                  + "There is a number "+(tile-25+1)+" etched into it.");
          timer.schedule(new GameInfoTimeout(), 3000);
        }
        room.tlItems.setAnimatedTile(this.item, 0);
      }
    }

    public void run() {
      while (this.tile++ != this.last) {
        room.tlItems.setAnimatedTile(this.item, this.tile);
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
      }
    }
  }

  private boolean door_locked_wait = false;
  private final boolean needKey(int dir) {
   MazeKey key = null;
    if (MazeMap.getLockDir(maze.cellCurrent, dir) == 0) return false; // not locked
    key = (MazeKey) maze.keys.elementAt(MazeMap.getLockKey(maze.cellCurrent, dir));
    // need key
    if (!door_locked_wait) {
      txtGameInfo.addElement("A door blocks your path.\n"
              +"There is a "+key.description+key.name
              +" lock with the number "+(MazeMap.getLockKey(maze.cellCurrent, dir)+1)+" etched into it.");
      door_locked_wait = true;
      timer.schedule(new DoorLockedTimeout(), 3000);
    }
    if (pc.inventory.indexOf(key) != -1) { // has the key
      pc.inventory.removeElement(key);
      room.doorOpen(dir);
      txtGameInfo.addElement("You use the "+key.name+" key to unlock the door.");
      timer.schedule(new DoorLockedTimeout(), 3000);
      key.cell1.lock -= key.lock1;
      key.cell2.lock -= key.lock2;
      return false;
    }
    return true;
  }

  private final boolean nextRoom() {
   int c = maze.cellCurrent.x;
   int r = maze.cellCurrent.y;
    // check if we need to move to next room
    if (!pc.collidesWith(room.tlDoors, false)) return false;
    if (pc.dir == LEFT) {
      if (needKey(MazeMap.LEFT)) return false;
      c--;
      pc.move(+room.getWidth()-2*room.tlBase.getCellWidth()-pc.getWidth()+4,0);
    } else if (pc.dir == RIGHT) {
      if (needKey(MazeMap.RIGHT)) return false;
      c++;
      pc.move(-room.getWidth()+2*room.tlBase.getCellWidth()+pc.getWidth()-4,0);
    } else if (pc.dir == UP) {
      if (needKey(MazeMap.UP)) return false;
      r--;
      pc.move(0,+room.getHeight()-2*room.tlBase.getCellHeight()-pc.getHeight()+16);
    } else if (pc.dir == DOWN) {
      if (needKey(MazeMap.DOWN)) return false;
      r++;
      pc.move(0,-room.getHeight()+2*room.tlBase.getCellHeight()+pc.getHeight()-16);
    }
    maze.cellCurrent = maze.Map[r][c];
    room = null;
    room = new MazeRoom(maze,tsMAP,tsGround,tsFringe,tsItems);
    room.insert(pc,0);
    return true;
  }

  public final void movePC(double dx, double dy) {
    if (!pc.idle) {
      pc.move(dx,dy);
      if (!nextRoom() && pc.collidesWith(room.tlFringe, false)) {
        pc.move(-dx,-dy);
      }
      room.setViewPort(viewX+pc.getX()+pc.getWidth()/2,viewY+pc.getY()+pc.getHeight()/2,viewW,viewH);
    }
  }

  public final void action_item(int c, int r) {
   // open
   int item = -(room.tlItems.getCell(c, r)+1);
    if (( maze.cellCurrent.items & (1 << item)) == 0) {
      maze.cellCurrent.items |= (1 << item);
      new AnimateItem(item);
    }
  }
  
  /**
   * Copy an int[] by value (deep copy).
   * Avoids use of Object.clone() method (copy by ref / shallow copy)
   * @param src
   * @return 
   */
  public static final int[] copyArray(int[] src) {
   int[] dst = new int[src.length];
    System.arraycopy(src, 0, dst, 0, src.length);
    return dst;
  }

  public static final Object[] copyArray(Object[] src) {
   Object[] dst = new Object[src.length];
    for (int i=0; i < dst.length; i++) {
      if (dst[i] != null) dst[i] = copyArray((int[]) src[i]);
    }
    return dst;
  }
}
